<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


Route::view('/', 'master')->name('main');
Route::get('/booklist', [PagesController::class, 'booklist'])->name('booklist');

Route::group(['middleware' => ['role:admin'], ['as' => '.']], function () {
    Route::resource('category', CategoryController::class)->middleware("permission:category managment");
    Route::resource('user',     UserController::class)->middleware("permission:user managment");
});

Route::group(['middleware' => ['role:admin|editor'], ['as' => '.']], function () {
    Route::resource('book', BookController::class)->middleware("permission:book managment");
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
