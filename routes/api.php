<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['role:admin']], function () {

    Route::get('category/list', [CategoryController::class, 'list'])->name('categorylist')->middleware("permission:category managment");
    Route::get('user/list', [UserController::class, 'list'])->name('userlist')->middleware("permission:user managment");

});

Route::group(['middleware' => ['role:admin|editor'] ], function () {

    Route::get('book/list', [BookController::class, 'list'])->name('userBooksList')->middleware("permission:book managment");

});

