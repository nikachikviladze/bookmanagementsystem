<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Book extends Model
{
    use HasFactory;

    protected $fillable  = [
        'title', 
        'description', 
        'user_id', 
        'image'
    ];

    public function User()
    {
      return $this->belongsTo(User::class);
    }
    public function category()
    {
      return $this->belongsToMany(Category::class);
    }
    public function image()
    {
      return URL::asset($this->image);
    }

}
