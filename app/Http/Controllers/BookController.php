<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use App\Models\Category;
use App\Models\User;
use App\Traits\BookTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class BookController extends Controller
{
    use BookTrait;

    public $categories;
    public $imageUrl;

    public function __construct()
    {
        $this->middleware('auth');

        $this->categories = Category::where('status', 1)->get();
    }

    public function uploadImage($file, $book=null)
    {
        if ($file) {
            
            $filename = Str::random(5).'-'. time().'-'.Str::random(5).'.'. $file->getClientOriginalExtension();
            
            $image = Image::make($file);
            
            Storage::disk('local')->put("private/books/{$filename}", (string) $image->encode());
            
            $this->imageUrl = "storage/private/books/{$filename}";

            self::destroyImage($book);            
        }
    }
    public static function destroyImage($book)
    {
        return ($book)? Storage::disk('local')->delete("private/books/". basename($book->image())) : null;
    }
    
    public function list()
    {
        $user = Auth::user();

        $books = ($user->hasRole("admin"))? Book::all() : auth()->user()->Book;

        return $this->DatatablesList($books);
    }

    public function index()
    {       
        return view('book.index');
    }

    public function create()
    {
        return view('book.create', ["categories"=>$this->categories]);
    }

    public function store(BookRequest $request)
    {
        self::uploadImage($request->image);

        $book = Book::create(["title"=>$request->title, 
                              "description"=>$request->description,
                              "image"=>$this->imageUrl,
                              "user_id"=>auth()->user()->id
                        ]);

        $book->category()->sync($request->category_id, false);

        return redirect()->route('book.index')->with("success", 'Create Successfuly');
    }

    public function edit(Book $book)
    {
        $this->authorize('edit', $book);

        return view('book.update', ["book"=>$book, "categories"=>$this->categories]);
    }

    public function update(BookRequest $request, Book $book)
    {
        $this->authorize('update', $book);

        $this->imageUrl = $book->image;

        self::uploadImage($request->image, $book);

        $book->update(["title"=>$request->title, 
                       "description"=>$request->description,
                       "image"=>$this->imageUrl
                    ]);
                    
        $book->category()->sync($request->category_id);

        return redirect()->route('book.edit', $book->id)->with("success", 'Update Successfuly');
    }

    public function destroy(Book $book)
    {
        $this->authorize('destroy', $book);

        self::destroyImage($book);

        $book->delete();

        return redirect()->route('book.index')->with("success", 'Deleted Successfuly');
    }
}