<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use DataTables;

class CategoryController extends Controller
{

    public function index()
    {
        return view('category.index');
    }
    public function list()
    {
        $results = Category::all();

        return Datatables::of($results)
                        ->editColumn('status', function ($result) {                
                            return ($result->status==1)? "Active" : "Disabled";
                        })
                        ->addColumn('action', function($result){
                            $update = route("category.edit", $result->id);
                            $destroy = route("category.destroy", $result->id);

                            return view('partials.datatables.action', compact("update", 'destroy'));
                        })
                        ->rawColumns(["action"])
                        ->make(true);

    }

    public function create()
    {
        return view('category.create');
    }

    public function store(CategoryRequest $request)
    {
        Category::create(['name'=>$request->name, "status"=>$request->status]);

        return redirect()->route('category.index')->with("success", 'Create Successfuly');
    }

    public function edit(Category $category)
    {
        return view('category.update', compact("category"));
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $category->update(['name'=>$request->name, "status"=>$request->status]);

        return redirect()->route('category.edit', $category->id)->with("success", 'Update Successfuly');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('category.index')->with("success", 'Deleted Successfuly');
    }
}
