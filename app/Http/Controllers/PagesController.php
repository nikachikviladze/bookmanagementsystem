<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use App\Traits\BookTrait;

class PagesController extends Controller
{
    use BookTrait;
    
    public function booklist()
    {
        $categories = Category::has('books')->where("status", 1)->pluck("id")->toArray();
        $ids = DB::table("book_category")->whereIn("category_id", $categories)->pluck("book_id")->toArray();
        $books = Book::whereIn("id", $ids)->get();

        return $this->DatatablesList($books);
    }
    
}
