<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DataTables;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public $roles;

    public function __construct()
    {
        $this->roles = Role::all();
    }
    public function index()
    {
        return view('user.index');
    }

    public function list()
    {
        $results = User::all();

        return Datatables::of($results)            
                        ->addColumn('action', function($result){
                            $update = route("user.edit", $result->id);
                            $destroy = route("user.destroy", $result->id);

                            return view('partials.datatables.action', compact("update", 'destroy'));
                        })
                        ->editColumn('created_at', function ($result) {
                            return $result->created_at->format('d/m/Y H:i');
                        })
                        ->addColumn('role', function($user){
                            $results = $user->roles;

                            return view('partials.datatables.list', compact("results"));
                        })
                        ->rawColumns(["role", "action"])
                        ->make(true);
    }

    public function create()
    {
        return view('user.create', ["roles"=>$this->roles]);
    }

    public function store(UserStoreRequest $request)
    {
        $role = Role::where("name", $request->role)->first();  
        
        $user = User::create([
            "name"=>$request->name, 
            "email"=>$request->email, 
            "password"=> Hash::make($request->password) 
        ]);

        $user->syncRoles($request->role);
        $user->syncPermissions($role->permissions);            

        return redirect()->route('user.index')->with("success", 'Create Successfuly');
    }

    public function edit(User $user)
    {
        return view('user.update', ["roles"=>$this->roles, "user"=>$user]);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update([
            "name" => $request->name, 
            "email" => $request->email, 
            "password"=> ($request->password)? Hash::make($request->password) : $user->password, 
        ]);

        $user->roles()->detach();
        $user->permissions()->detach();

        $role = Role::where("name", $request->role)->first();
 
        $user->assignRole($request->role);
        $user->syncPermissions($role->permissions);            

        return redirect()->route('user.edit', $user->id)->with("success", 'Update Successfuly');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.index')->with("success", 'Deleted Successfuly');
    }
}
