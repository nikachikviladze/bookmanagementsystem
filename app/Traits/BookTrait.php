<?php

namespace App\Traits;
use DataTables;
use Auth;

trait BookTrait
{
    static function DatatablesList($books)
    {
        return Datatables::of($books)
                        ->editColumn("category", function ($model) {
                            $results = $model->category;
                            return view("partials.datatables.list", compact("results"));
                        })
                        ->editColumn("image", function ($result) {
                            if (Auth::check() && $result->image) {
                                return view("partials.datatables.image", compact("result"));
                            }
                        })
                        ->addColumn("publisher", function ($result) {
                            return $result->user ? $result->user->name : null;
                        })
                        ->addColumn("action", function ($result) {
                            $update = route("book.edit", $result->id);
                            $destroy = route("book.destroy", $result->id);
                            return view("partials.datatables.action", compact("update", "destroy"));
                        })
                        ->rawColumns(["publisher", "action", "category", "image"])
                        ->make(true);
    }
}
