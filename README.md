## გაშვება

- .env.example to .env
- connect database
- composer update
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan storage:link
- php artisan serv

## როლები და პერმიშენები

- საიტის გაშვებისას იქმნება მომხმარებლის ორი როლი admin და editor
- იქმნება პერმიშენები book managment, category managment, user managment
- ადმინის როლი მქონეს ენიჭება ყველა პერმიშენი. editorს მხოლოდ book managment 
- დარეგისტრირებულ მომხმარებელს ავტომატურად ენიჭება editor როლი
- ადმინის როლის მქონეს უფლება აქვს სხვა მომხმარებელს მიანიჭოს admin
- ადმინს უფლება აქვს დაარედაქტიროს და წაშალოს ყველაფერი. editor უზერს უფლება აქვს წაშალოს და რედაქტირება გაუკეთოს მხოლოდ მის მიერ დადებულებს.