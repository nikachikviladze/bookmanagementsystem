<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Category::first()){
            Category::factory(5)->has(Book::factory()->count(rand(1,3)))->create();
        }
        
    }
}
