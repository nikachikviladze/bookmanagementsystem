<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Role::first()){
            $role1 = Role::create(['name' => 'admin']);
            $role2 = Role::create(['name' => 'editor']);
    
            $permission1 = Permission::create(['name' => 'book managment']);
            $permission2 = Permission::create(['name' => 'category managment']);
            $permission3 = Permission::create(['name' => 'user managment']);
    
            $role1->syncPermissions([$permission1, $permission2, $permission3]);
            $role2->syncPermissions([$permission1]);
    
            $permission1->syncRoles([$role1, $role2]);
            $permission2->syncRoles([$role1]);
            $permission3->syncRoles([$role1]);
    
            $user = User::first();
    
            $user->assignRole('admin');
            $user->syncPermissions(['book managment', 'category managment', 'user managment']);
        }
        
    }
}
