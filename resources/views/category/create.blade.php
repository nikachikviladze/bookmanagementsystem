@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
         <li class="breadcrumb-item"><a href="{{route('category.index')}}">Categories</a></li>
      </ol>
   </nav>

   <div class="row justify-content-center">
      <div class="col-md-12">
         @include('layouts.messages')
         <div class="card">
            <div class="card-header">Add New Category</div>
            <div class="card-body">
               <form action="{{route('category.store')}}" method="post" enctype="multipart/form-data">
                  @csrf                       
                  <div class="form-group">
                     <label for=""><b>Name</b></label>
                     <input value="{{old('name')}}" required type="text" class="form-control" name="name">
                  </div>

                  <div class="form-check">
                     <input class="form-check-input"  value="1" type="radio" name="status" id="flexRadioDefault1" checked>
                     <label class="form-check-label" for="flexRadioDefault1">Active</label>
                   </div>
                   <div class="form-check">
                     <input class="form-check-input" value="0" type="radio" name="status" id="flexRadioDefault2">
                     <label class="form-check-label" for="flexRadioDefault2">Disabled</label>
                   </div>
                   
                  <div class="mt-3 form-group">                                
                     <button type="submit" class="btn btn-default btn-outline-secondary">Send</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection