@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
      </ol>
   </nav>


   <div class="row justify-content-center">
      <section class="content">
         @include('layouts.messages')
         <div class="box">
            <div class="box-header mb-2">
               <a href="{{route('category.create')}}" class="btn btn-secondary pull-right">Add Category</a>
               <h3 class="box-title">Categories List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="booklist" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </section>
   </div>
</div>
@endsection
@section('js')
<script>
   $(function () {
       $("#booklist").DataTable({
           processing: true,
           aaSorting: [[0, "desc"]],
           ajax: "{{route('categorylist')}}",
           columns: [
               { data: "name", name: "category.name" },
               { data: "status", name: "category.status" },
               { data: "action", orderable: false, searchable: false },
           ],
       });
   });
</script>    
@endsection