<a class="btn btn-secondary btn-sm mb-1" href="{{$update}}">Edit</a>
<form action="{{$destroy}}" method="POST" class="d-inline-block">
@csrf
<input type="hidden" name="_method" value="DELETE">
<button type="submit" class="btn btn-danger btn-sm"
    onclick="return confirm(\'Are You Sure Want to Delete?\')">Delete</button>
</form>