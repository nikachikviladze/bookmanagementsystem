@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
      </ol>
   </nav>


   <div class="row justify-content-center">
      <section class="content">
         @include('layouts.messages')
         <div class="box">
            <div class="box-header mb-2">
               <a href="{{route('book.create')}}" class="btn btn-secondary pull-right">Add Book</a>
               <h3 class="box-title">Books List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="booklist" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>Id</th>
                          <th>Image</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Categories</th>
                          <th>Publisher</th>
                          <th>Action</th>
                      </tr>
                  </thead>
              </table>
            </div>
         </div>
      </section>
   </div>
</div>
@endsection
@section('js')
<script>
    $(function () {
        $("#booklist").DataTable({
            processing: true,
            aaSorting: [[0, "desc"]],
            ajax: "{{route('userBooksList')}}",
            columns: [
               { data: "id", name: "book.id" },
               { data: "image", orderable: false, searchable: false, name: "book.image" },
               { data: "title", name: "book.title" },
               { data: "description", name: "book.description" },
               { data: "category", name: "book.category" },
               { data: "publisher", name: "book.publisher" },
               { data: "action", orderable: false, searchable: false },
            ],
        });
    });
</script>    
@endsection