@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
         <li class="breadcrumb-item"><a href="{{route('book.index')}}">Books</a></li>
      </ol>
   </nav>

   <div class="row justify-content-center">
      <div class="col-md-12">
         @include('layouts.messages')
         <div class="card">
            <div class="card-header">Add New Book</div>
            <div class="card-body">
               <form action="{{route('book.store')}}" method="post" enctype="multipart/form-data">
                  @csrf                       
                  <div class="form-group">
                     <label for="title"><b>Title</b></label>
                     <input value="{{old('title')}}" id="title" required type="text" class="form-control" name="title" >
                  </div>
                  <div class="form-group">
                     <label for="description"><b>Description</b></label>
                     <textarea id="description" required class="form-control" name="description">{{old('description')}}</textarea>
                  </div>
                  <div class="form-group">
                     <label for=""><b>Categories</b></label>
                     <select required name="category_id[]" class="form-control" multiple>
                        <option disabled selected>Choose Category</option>

                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(old('category_id') && in_array($category->id, old('category_id')))) selected @endif>{{$category->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="image"><b>Cover Image</b></label>
                     <input id="image" type="file" class="form-control" name="image" >
                  </div>

                  <div class="mt-3 form-group">                                
                     <button type="submit" class="btn btn-default btn-outline-secondary">Send</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection