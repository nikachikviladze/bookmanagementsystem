@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
         <li class="breadcrumb-item"><a href="{{route('book.index')}}">Books</a></li>
      </ol>
   </nav>
   <div class="row justify-content-center">
      <div class="col-md-12">
         @include('layouts.messages')
         <div class="card">
            <div class="card-header">Update Book</div>
            <div class="card-body">
               <form action="{{route('book.update', $book->id)}}" method="post" enctype="multipart/form-data">
                  @csrf        
                  @method('PUT')               
                  <div class="form-group">
                     <label for="title"><b>Title</b></label>
                     <input value="{{$book->title}}" id="title" required type="text" class="form-control" name="title" >
                  </div>
                  <div class="form-group">
                     <label for="description"><b>Description</b></label>
                     <textarea id="description" required class="form-control" name="description">{{$book->description}}</textarea>
                  </div>
                  <div class="form-group">
                     <label for=""><b>Categories</b></label>
                     <select required name="category_id[]" class="form-control" multiple>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}"  @if(in_array($category->id, $book->category->pluck('id')->toArray()))) selected @endif>{{$category->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="image"><b>Cover Image</b></label>
                     @if($book->image) <img src="{{$book->image()}}" width="150" class="d-block mt-1 mb-1"> @endif
                     <input id="image" type="file" class="form-control" name="image">
                  </div>

                  <div class="mt-3 form-group">                                
                     <button type="submit" class="btn btn-default btn-outline-secondary">Send</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection