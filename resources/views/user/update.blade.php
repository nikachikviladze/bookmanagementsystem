@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
         <li class="breadcrumb-item"><a href="{{route('user.index')}}">Users</a></li>
      </ol>
   </nav>
   <div class="row justify-content-center">
      <div class="col-md-12">
         @include('layouts.messages')
         <div class="card">
            <div class="card-header">Update User</div>
            <div class="card-body">
               <form action="{{route('user.update', $user->id)}}" method="post" enctype="multipart/form-data">
                  @csrf        
                  @method('PUT')               
                  <div class="form-group">
                     <label for="name"><b>Name</b></label>
                     <input id="name" value="{{$user->name}}" required type="text" class="form-control" name="name" >
                  </div>
                  <div class="form-group">
                     <label for="email"><b>Email</b></label>
                     <input id="email" value="{{$user->email}}" required type="email" class="form-control" name="email" >
                  </div>
                  <div class="form-group">
                     <label for="password"><b>Password</b></label>
                     <input type="password" class="form-control" name="password" >
                  </div>
                  <div class="form-group">
                     <label for="password"><b>Password Confirmation</b></label>
                     <input type="password" class="form-control" name="password_confirmation" >
                  </div>
                  <div class="form-group">
                     <label for=""><b>Role</b></label>
                     <select required name="role[]" class="form-control" multiple>

                        @foreach($roles as $role)
                        <option value="{{$role->name}}" @if(in_array($role->id, $user->roles->pluck('id')->toArray()))) selected @endif>{{$role->name}}</option>

                        @endforeach
                     </select>
                  </div>

                  <div class="mt-3 form-group">                                
                     <button type="submit" class="btn btn-default btn-outline-secondary">Send</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection