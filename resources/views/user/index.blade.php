@extends('layouts.app')
@section('content')
<div class="container">
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{route('main')}}">Home</a></li>
         <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
      </ol>
   </nav>


   <div class="row justify-content-center">
      <section class="content">
         @include('layouts.messages')
         <div class="box">
            <div class="box-header mb-2">
               <a href="{{route('user.create')}}" class="btn btn-secondary pull-right">Add User</a>
               <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="booklist" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Roles</th>
                        <th>Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </section>
   </div>
</div>
@endsection
@section('js')
<script>
   $(function () {
       $("#booklist").DataTable({
           processing: true,
           aaSorting: [[0, "desc"]],
           ajax: "{{route('userlist')}}",
           columns: [
               { data: "name", name: "user.name" },
               { data: "email", name: "user.email" },
               { data: "created_at", name: "user.created_at" },
               { data: "role", name: "user.role" },
               { data: "action", orderable: false, searchable: false },
           ],
       });
   });
</script>    
@endsection