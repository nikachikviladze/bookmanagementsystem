@if(Session::has('success'))
<div class="alert alert-success">{{ session::get('success') }}</div>
@endif

@if(session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif


@if(count($errors)>0)
<ul class="alert alert-danger">
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
</ul>
@endif
