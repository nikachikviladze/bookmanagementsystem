@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        @can("user managment")
        <div class="col-md-4">
            <div class="card p-5 text-center">
                 <a href="{{route('user.index')}}">Users</a> 
            </div>
        </div>
        @endcan
        @can("category managment")
        <div class="col-md-4">
            <div class="card p-5 text-center">
                 <a href="{{route('category.index')}}">Categories</a> 
            </div>
        </div>
        @endcan
        @can("book managment")
        <div class="col-md-4">
            <div class="card p-5 text-center">
                 <a href="{{route('book.index')}}">My Books</a> 
            </div>
        </div>
        @endcan

    </div>
</div>
@endsection
