@extends('layouts.app')
@section("content")


            <div class="container">
                <div class="row justify-content-center">
                    <section class="content">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Book List</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="booklist" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Categories</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        
@endsection
@section('js')
<script>
    $(function () {
        $("#booklist").DataTable({
            processing: true,
            aaSorting: [[0, "desc"]],
            ajax: "{{route('booklist')}}",
            columns: [
                { data: "id", name: "book.id" },
                { data: "image", orderable: false, searchable: false, name: "book.image" },
                { data: "title", name: "book.title" },
                { data: "description", name: "book.description" },
                { data: "category", name: "book.category" },
            ],
        });
    });
</script>    
@endsection